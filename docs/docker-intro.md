# Docker Intro

When we run an app or a service in a docker container:

- the app/service runs in a lightweight isolated environment, like a VM but lighter.
- a docker container is (or should be) stateless, so it can be
  restarted with no issues.

While a VM takes ~30s to start, a container can be started in ~1 second or less.
This cheap start/stop cycle, together with smaller images, less overhead in
the virtualized storage and CPU
make containers way more convenient than VMs.


!!! Note

    Since containers are so cheap to run, they are "disposable".
    You always start a container from a clean image (stateless).
    With VM, instead, it was common to "keep a VM around" and boot into
    it several times (stateful).


## Glossary

### Docker Host

The computer where the container runs (for example our laptop or a server).
The host can also be a managed cloud service.

For example
*Azure Container Instance* runs a container for us, freeing us from the need to
maintain a server to run containers.

### Dockerfile

The "recipe" to build a docker image.

### Docker image (or container image)

The "image" (a file) containing the environment built following the dockerfile.

The image format is [Open Container Image](https://opencontainers.org/) (OCI) format.
This is an open format that can be run by any OCI-compatible runtime.

The "Docker engine" is an OCI-compatible runtime.
Another is `containerd` used by Kubernetes.

People commonly call the image a "docker image", even if it is a bit inaccurate.

### Docker container

Running a container image we obtain a "docker container". This is the "live" executed env.

Normally we destroy the container as soon as the container stops running.
We can avoid to destroy the "live" image even after the container stops running,
although this is an anti-pattern, used only for debugging/inspecting a container.
