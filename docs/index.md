# Welcome!


Welcome to the Docker Tutorial!

Before you start make sure you have docker installed.

## Install Docker on Mac OS
Go to the [download page](https://docs.docker.com/desktop/mac/install/) to install it.

## Install Rancher Desktop on Windows
- Requirements: WSL2 already installed and configured with a Linux distro.

- Go to the [Rancher Desktop page](https://rancherdesktop.io/) and click on __Download Windows__.
It will download an Installer (e.g. Rancher.Desktop.Setup.1.7.0).
- Follow the installer instructions.
- Reboot your pc.
- Open Rancher Desktop. Look at the Windows application bar, at the bottom left
side: you will have the __Rancher Desktop icon__. Right click on it and
select __Preferences__.
- Open __WSL Preferences__ on the right side and select your Linux disto (e.g. Ubuntu).
Click on Apply.
- Open a WSL Terminal.
- Install docker on WSL:
```
sudo apt-get update
sudo apt install docker.io
```
- Check docker version installed with:
```
docker version
```
you will probably get a permission denied error:
```
Got permission denied while trying to connect to the Docker daemon socket at
unix:///var/run/docker.sock: Get 
"http://%2Fvar%2Frun%2Fdocker.sock/v1.24/containers/json": dial unix 
/var/run/docker.sock: connect: permission denied
```
- If you run `docker ps`, you will have the same error: the user has no permission to 
run Docker service.
- Add permissions with:
```
sudo addgroup --system docker
sudo adduser $USER docker
newgrp docker
# And something needs to be done so $USER always runs in group `docker` on the WSL
sudo chown root:docker /var/run/docker.sock
sudo chmod g+w /var/run/docker.sock
```
- Close WSL Terminal and re-open it. Check if `docker ps` works. You should see the list
 of all Docker containers.

- P.S. to use Docker in WSL Terminal you need to have opened Rancher Desktop before.


## What's next
To start go to [Docker Intro](docker-intro.md).