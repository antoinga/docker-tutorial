# Anatomy of a Dockerfile

We have seen that in general we don't want to keep around a container
after it is run because it has a potentially modified local filesystem.

But [what if](https://what-if.xkcd.com/157/) we *want* to modify an existing docker image?

## A minimal example

The proper way to "modify" a docker image is to build a new image
using an existing one as basis.

A Dockerfile is the recipe to build a docker image. All the modifications
to the base image done in the dockerfile are "persisted" in the newly
created image.

Here it is a minimal example.


``` dockerfile title="Dockerfile" linenums="1"
# (1)
FROM debian:testing-slim

# (2)
RUN mkdir /pippo

# (3)
COPY script.sh /app/

# (4)
WORKDIR /app

# (5)
ENTRYPOINT ["bash"]
CMD ["script.sh"]
```

1.  This is the starting image, a "slim" version of "Debian Testing".
2.  With `RUN` we can run any command in the docker container. Here,
    we do some random changes to the filesystem.
3.  Just for fun, we are copying a file from the host to the container.
4.  To change the current directory in a Dockerfile
    you need to use `WORKDIR`.  You cannot use `cd` in a Dockerfile because
    each command in `RUN` is executed in a sub-shell (so any dir change
    would be reverted at the end of the command).
5.  ENTRYPOINT and RUN define the default command to be run when we call
    `docker run`. These can be changed at runtime. To override the `CMD`,
    add the new command as last argument to `docker run`.
    To override the entrypoint use the `--entrypoint` option.

Now let's save a simple `script.sh`:

``` bash
#!/bin/bash

echo "I'm a bash script, yey!"
```

Next, we can build the image:

``` bash
docker build -f Dockerfile -t bash-script .
```

Remember that you can see the new image with `docker image ls`. To run the image:

``` bash
docker run --rm -it bash-script  # (1)
```

1. The complete command run here is the concatenation of `ENTRYPOINT` and
   `CMD`. In this example it is  `bash script.sh`.


Any additional arguments to `docker run` will override the `CMD`:

``` bash
docker run --rm -it bash-script -c ls  # (1)
```

1.  The complete command here is `bash -c ls`

To override the entrypoint, we pass the `--entrypoint` option.
Here it is a complete example, using variables for clarity:

``` bash
IMAGE_NAME="bash-script"
ENTRYPOINT="apt-get"
CMD="moo"
docker run --rm -it --entrypoint $ENTRYPOINT $IMAGE_NAME $CMD  # (1)
```

1.  Note that `--entrypoint` must be passed before the image name!


??? Tip "Result"

    ```
                     (__)
                     (oo)
               /------\/
              / |    ||
             *  /\---/\
                ~~   ~~
    ..."Have you mooed today?"...
    ```

    What's going on here?


??? Danger "Danger: serious fun ahead!"

    ``` bash
    IMAGE_NAME="bash-script"
    ENTRYPOINT="telnet"
    CMD="towel.blinkenlights.nl"
    docker run --rm -it --entrypoint $ENTRYPOINT $IMAGE_NAME $CMD
    ```

    Unfortunately, the previous command gives an error. Can you fix
    the Dockerfile to make the previous command run?

